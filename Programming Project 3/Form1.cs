﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Security;

namespace Programming_Project_3
{
    public partial class TextMangler : Form
    {
        // create list to hold all data to be processed
        public List<String> textToProcess = new List<String>();
        public TextMangler()
        {
            InitializeComponent();
        }

        private void FileLoadButton_Click(object sender, EventArgs e)
        {
            // create instance to capture file once button is clicked
            OpenFileDialog openAFile = new OpenFileDialog();
            // title window
            openAFile.Title = "Open File to Process.";
            // setup string to output to file
            List<String> fileOutput = new List<String>();
            // create path based on logged in user to save file in Windows
            String docSavePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            // confirm we can successfully open the file dialog
            if (openAFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // setup stream to read file from
                    var streamReader = new StreamReader(openAFile.FileName);
                    // set variable to consume entire stream from file
                    String textToMangle = streamReader.ReadToEnd();
                    // set text to lowercase
                    textToMangle = textToMangle.ToLower();
                    // put text all lowercase into display box on form
                    lowerCaseTextBox.Text = textToMangle;
                    // process test to add it to the public list array
                    textToProcess = ChopUpString(textToMangle);
                    // sort list in order so we get a-z by first letter
                    textToProcess = textToProcess.OrderBy(x => x).ToList();
                    // write out first word by alpha
                    firstWordBox.Text = textToProcess[0];
                    // add word to array to be written to file
                    fileOutput.Add(firstWordLabel.Text + ": " + textToProcess[0]);
                    // write out last word by alpha
                    lastWordBox.Text = textToProcess.Last();
                    // add word to array to be written to file
                    fileOutput.Add(lastWordLabel.Text + ": " + textToProcess.Last());
                    // write out longest word by # of characters
                    longestWordBox.Text = LongWordFinder(textToProcess);
                    // add word to array to be written to file
                    fileOutput.Add(longestWordLabel.Text + ": " + LongWordFinder(textToProcess));
                    // identify word containing the most vowels and write to text box
                    mostVowelsBox.Text = VowelestWordFinder(textToProcess);
                    // add word to array to be written to file
                    fileOutput.Add(mostVowelsLabel.Text + ": " + VowelestWordFinder(textToProcess));
                    // last we add the lowercase text to the array as well to mimic the GUI layout
                    fileOutput.Add(lowerCaseLabel.Text + ": " + textToMangle);
                    // create streamwriter to save file with name to the previously defined location
                    using (StreamWriter writeTheFile = new StreamWriter(Path.Combine(docSavePath, "fileResults.txt")))
                    {
                        // consume each item in the array
                        foreach (String line in fileOutput)
                        {
                            // add each item to the file in order
                            writeTheFile.WriteLine(line);
                        }
                    }
                    // notify the end user what just happened
                    MessageBox.Show("A file with the results displayed was just created in your MyDocuments folder.");
                }
                // suggested error handling
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" + $"Details:\n\n{ex.StackTrace}");
                }
            }
        }
        // method to turn a string into an array to process
        private List<String> ChopUpString(String text)
        {
            // we are splitting the string into words keying off of the space
            string [] temporaryList = text.Split(' ');
            // returning our string array and converting it to list array
            return temporaryList.ToList();
        }
        // method to count the length of each string in the array and return the longest
        private String LongWordFinder(List<String> text)
        {
            // set variables for item in array and length of first item
            int index = 0;
            int elementLength = text[0].Length;
            // for loop to parse every item in the array passed to the method
            for (int counter = 1; counter < text.Count; counter++)
            {
                // compare current "longest word" to the next item in the array
                if (text[counter].Length > elementLength)
                {
                    // if the next item in the array is longer, we have a new "longest word"
                    index = counter; elementLength = text[counter].Length;
                }
            }
            // return the winner from the length contest
            return text[index];
        }
        // method to find the word with the most vowels
        private String VowelestWordFinder(List<String> text)
        {
            // define variables for the string of the word, highest vowel count and current vowel count
            String vowelestWord = "";
            int maxVowels = 0;
            int countVowels = 0;
            // create a hash to store our characters we consider vowels
            var vowels = new HashSet<char> { 'a', 'e', 'i', 'o', 'u' };
            // iterate through everything in the passed array
            foreach (string word in text)
            {
                // iterate through each character of the provided string
                for (int counter = 0; counter < word.Length; counter++)
                {
                    // if the character we iterate matches our hash we increment the vowel count
                    if (vowels.Contains(word[counter]))
                    {
                        countVowels++;
                    }
                }
                // check to see if we have a new champion of vowel counts
                if (countVowels > maxVowels)
                {
                    // set new word with the most vowels to our highest vowel count variables and reset the countVowels variable
                    vowelestWord = word;
                    maxVowels = countVowels;
                    countVowels = 0;
                }
                // reset for each string that gets counted
                countVowels = 0;
            }
            // return the string that was saved as the highest vowel count
            return vowelestWord;
        }
    }
}