﻿namespace Programming_Project_3
{
    partial class TextMangler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextMangler));
            this.fileLoadButton = new System.Windows.Forms.Button();
            this.firstWordBox = new System.Windows.Forms.TextBox();
            this.lastWordBox = new System.Windows.Forms.TextBox();
            this.firstWordLabel = new System.Windows.Forms.Label();
            this.lastWordLabel = new System.Windows.Forms.Label();
            this.longestWordLabel = new System.Windows.Forms.Label();
            this.longestWordBox = new System.Windows.Forms.TextBox();
            this.mostVowelsLabel = new System.Windows.Forms.Label();
            this.mostVowelsBox = new System.Windows.Forms.TextBox();
            this.lowerCaseLabel = new System.Windows.Forms.Label();
            this.lowerCaseTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // fileLoadButton
            // 
            this.fileLoadButton.Location = new System.Drawing.Point(15, 237);
            this.fileLoadButton.Name = "fileLoadButton";
            this.fileLoadButton.Size = new System.Drawing.Size(75, 46);
            this.fileLoadButton.TabIndex = 0;
            this.fileLoadButton.Text = "LOAD FILE";
            this.fileLoadButton.UseVisualStyleBackColor = true;
            this.fileLoadButton.Click += new System.EventHandler(this.FileLoadButton_Click);
            // 
            // firstWordBox
            // 
            this.firstWordBox.Location = new System.Drawing.Point(157, 9);
            this.firstWordBox.Name = "firstWordBox";
            this.firstWordBox.ReadOnly = true;
            this.firstWordBox.Size = new System.Drawing.Size(252, 20);
            this.firstWordBox.TabIndex = 1;
            // 
            // lastWordBox
            // 
            this.lastWordBox.Location = new System.Drawing.Point(157, 46);
            this.lastWordBox.Name = "lastWordBox";
            this.lastWordBox.ReadOnly = true;
            this.lastWordBox.Size = new System.Drawing.Size(252, 20);
            this.lastWordBox.TabIndex = 2;
            // 
            // firstWordLabel
            // 
            this.firstWordLabel.AutoSize = true;
            this.firstWordLabel.Location = new System.Drawing.Point(12, 9);
            this.firstWordLabel.Name = "firstWordLabel";
            this.firstWordLabel.Size = new System.Drawing.Size(123, 13);
            this.firstWordLabel.TabIndex = 3;
            this.firstWordLabel.Text = "First Word Alphabetically";
            // 
            // lastWordLabel
            // 
            this.lastWordLabel.AutoSize = true;
            this.lastWordLabel.Location = new System.Drawing.Point(12, 46);
            this.lastWordLabel.Name = "lastWordLabel";
            this.lastWordLabel.Size = new System.Drawing.Size(124, 13);
            this.lastWordLabel.TabIndex = 4;
            this.lastWordLabel.Text = "Last Word Alphabetically";
            // 
            // longestWordLabel
            // 
            this.longestWordLabel.AutoSize = true;
            this.longestWordLabel.Location = new System.Drawing.Point(12, 89);
            this.longestWordLabel.Name = "longestWordLabel";
            this.longestWordLabel.Size = new System.Drawing.Size(74, 13);
            this.longestWordLabel.TabIndex = 5;
            this.longestWordLabel.Text = "Longest Word";
            // 
            // longestWordBox
            // 
            this.longestWordBox.Location = new System.Drawing.Point(157, 86);
            this.longestWordBox.Name = "longestWordBox";
            this.longestWordBox.ReadOnly = true;
            this.longestWordBox.Size = new System.Drawing.Size(252, 20);
            this.longestWordBox.TabIndex = 6;
            // 
            // mostVowelsLabel
            // 
            this.mostVowelsLabel.AutoSize = true;
            this.mostVowelsLabel.Location = new System.Drawing.Point(12, 126);
            this.mostVowelsLabel.Name = "mostVowelsLabel";
            this.mostVowelsLabel.Size = new System.Drawing.Size(139, 13);
            this.mostVowelsLabel.TabIndex = 7;
            this.mostVowelsLabel.Text = "Word With the Most Vowels";
            // 
            // mostVowelsBox
            // 
            this.mostVowelsBox.Location = new System.Drawing.Point(157, 123);
            this.mostVowelsBox.Name = "mostVowelsBox";
            this.mostVowelsBox.ReadOnly = true;
            this.mostVowelsBox.Size = new System.Drawing.Size(252, 20);
            this.mostVowelsBox.TabIndex = 8;
            // 
            // lowerCaseLabel
            // 
            this.lowerCaseLabel.AutoSize = true;
            this.lowerCaseLabel.Location = new System.Drawing.Point(12, 162);
            this.lowerCaseLabel.Name = "lowerCaseLabel";
            this.lowerCaseLabel.Size = new System.Drawing.Size(83, 13);
            this.lowerCaseLabel.TabIndex = 10;
            this.lowerCaseLabel.Text = "Lowercase Text";
            // 
            // lowerCaseTextBox
            // 
            this.lowerCaseTextBox.Location = new System.Drawing.Point(157, 162);
            this.lowerCaseTextBox.Multiline = true;
            this.lowerCaseTextBox.Name = "lowerCaseTextBox";
            this.lowerCaseTextBox.ReadOnly = true;
            this.lowerCaseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.lowerCaseTextBox.Size = new System.Drawing.Size(252, 121);
            this.lowerCaseTextBox.TabIndex = 11;
            this.lowerCaseTextBox.Text = "Text Will Load Here.";
            // 
            // TextMangler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 297);
            this.Controls.Add(this.lowerCaseTextBox);
            this.Controls.Add(this.lowerCaseLabel);
            this.Controls.Add(this.mostVowelsBox);
            this.Controls.Add(this.mostVowelsLabel);
            this.Controls.Add(this.longestWordBox);
            this.Controls.Add(this.longestWordLabel);
            this.Controls.Add(this.lastWordLabel);
            this.Controls.Add(this.firstWordLabel);
            this.Controls.Add(this.lastWordBox);
            this.Controls.Add(this.firstWordBox);
            this.Controls.Add(this.fileLoadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TextMangler";
            this.Text = "Text Mangler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fileLoadButton;
        private System.Windows.Forms.TextBox firstWordBox;
        private System.Windows.Forms.TextBox lastWordBox;
        private System.Windows.Forms.Label firstWordLabel;
        private System.Windows.Forms.Label lastWordLabel;
        private System.Windows.Forms.Label longestWordLabel;
        private System.Windows.Forms.TextBox longestWordBox;
        private System.Windows.Forms.Label mostVowelsLabel;
        private System.Windows.Forms.TextBox mostVowelsBox;
        private System.Windows.Forms.Label lowerCaseLabel;
        private System.Windows.Forms.TextBox lowerCaseTextBox;
    }
}

